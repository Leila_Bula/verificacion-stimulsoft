<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

## Description

- Initial configuration
## Clonar repositorio

```bash
$ git clone https://gitlab.com/Leila_Bula/verificacion-stimulsoft.git
```

## Rename the file .env.example to .env and write in it the port like below

```bash
PORT=9903
```

## Install dependencies with the followed command

```bash
$ yarn
```

## Start the server with the command below

```bash
$ yarn start
```
## Request to endpoint below with the json contented in file "body.json" as request body
```bash
POST: http://localhost:9903/api/reporter/generate/doc
```