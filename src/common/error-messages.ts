export function ExistUK(entidad: string, campo: string): string {
    return 'Ya existe un' + entidad + ' con este ' + campo + ', intente nuevamente con otro valor';
}