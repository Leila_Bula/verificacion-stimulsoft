export function formatName(name: string): string {
    return name && name.includes('.') ? name.split('.')[0] : name;
}