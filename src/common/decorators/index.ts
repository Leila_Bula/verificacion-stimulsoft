export {IsId} from './is-id.decorator';
export {ValidateObject} from './validate-object.decorator';
export {ValidateObjectOptional} from './validate-object-optional.decorator';
export {PostS} from './post-s.decorator';