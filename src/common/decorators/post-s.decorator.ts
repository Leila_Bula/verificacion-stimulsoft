import {applyDecorators, HttpCode, Post} from '@nestjs/common';

export function PostS(...arg: any[]) {
    return applyDecorators(
        Post(arg),
        HttpCode(200)
    );
}