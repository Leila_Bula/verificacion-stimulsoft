import {applyDecorators} from '@nestjs/common';
import {IsInt, Min} from 'class-validator';

export function IsId() {
    return applyDecorators(
        IsInt(),
        Min(0)
    );
}