import {applyDecorators} from '@nestjs/common';
import {Type} from 'class-transformer';
import {IsInstance, IsObject, IsOptional} from 'class-validator';

export function ValidateObjectOptional(T: any) {
    return applyDecorators(
        Type(() => T),
        IsOptional(),
        IsObject(),
        IsInstance(T)
    );
}