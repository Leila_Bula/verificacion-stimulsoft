import {applyDecorators} from '@nestjs/common';
import {Type} from 'class-transformer';
import {ValidateNested} from 'class-validator';
import {ValidationOptions} from 'class-validator/types/decorator/ValidationOptions';

export function ValidateObject(T: any, validationOptions?: ValidationOptions) {
    return applyDecorators(
        Type(() => T),
        ValidateNested(validationOptions ?? {})
    );
}