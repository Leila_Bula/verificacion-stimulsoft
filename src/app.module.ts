import * as Joi from 'joi';
import {Module} from '@nestjs/common';
import {ConfigModule} from '@nestjs/config';
import config from './config';
import {environments} from './enviroments';
import {ReporterModule} from './reporter/reporter.module';

@Module({
    imports: [
        ConfigModule.forRoot({
            envFilePath: process.env.NODE_ENV ? environments[process.env.NODE_ENV] : '.env',
            load: [config],
            isGlobal: true,
            validationSchema: Joi.object({})
        }),
        ReporterModule,
    ]
})
export class AppModule {
}
