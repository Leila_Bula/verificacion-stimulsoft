export class Reporte {
    name: string;
    base64: string;
    type: string;

    constructor(data: string | Buffer, name?: string, type?: string) {
        if (Buffer.isBuffer(data)) {
            this.base64 = data.toString('base64');
        } else {
            this.base64 = data;
        }
        this.name = name || 'reporte.pdf';
        this.type = type || 'application/pdf';
    }
}