export {DataCampos} from './data-campos.interface';
export {Reporte} from './reporte.class';
export {ReportFormat, MimeTypes} from './report-format.enum';