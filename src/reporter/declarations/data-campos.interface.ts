import {QueryBuilderS} from './query-builder-s.interface';

export interface DataCampos {
    table_name: string,
    campos: string[],
    queryBuilder: QueryBuilderS
}