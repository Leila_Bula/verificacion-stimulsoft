import {IsArray, IsString} from 'class-validator';

export class ExportInfo {
    @IsString()
    table_name: string;

    @IsString({each: true})
    @IsArray()
    cabeceras: string[];

    @IsArray()
    datos: any[];
}