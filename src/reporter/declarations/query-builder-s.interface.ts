export interface QueryBuilderS {
    parte1: string;
    parte2: string;
    campos: object;
    params: string[];
    alias: string;
    params_o?: string[];
    parteF?: string;
}