import {Body, Controller, Post} from '@nestjs/common';
import {ReporterService} from './services/reporter.service';
import {GenerateDocumentDto} from './dto';
import {ApiTags} from '@nestjs/swagger';
import {Reporte} from './declarations';

@ApiTags('Reporter')
@Controller('reporter')
export class ReporterController {
    constructor(private readonly reporterService: ReporterService) {
    }

/*
    @Post('generate')
    async generateReport(@Body() generateReporteDTO: GenerateReportDto): Promise<Reporte> {
        return await this.reporterService.generateReport(generateReporteDTO);
    }
*/

    @Post('generate/doc')
    async generateDoc(@Body() generateDocumentDto: GenerateDocumentDto): Promise<Reporte> {
        return await this.reporterService.generateDocument(generateDocumentDto);
    }
}