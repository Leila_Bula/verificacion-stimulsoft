import {Template} from '../entities';
import {IsDefined, IsObject} from 'class-validator';
import {GenerateBaseDto} from './generate-base.dto';
import {Type} from 'class-transformer';

export class GenerateDocumentDto extends GenerateBaseDto {
    @IsObject()
    @Type(()=>Template)
    template: Template;

    @IsObject()
    @IsDefined()
    data: any;
}
