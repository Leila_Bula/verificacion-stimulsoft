import {GenerateBaseDto} from './generate-base.dto';
import {ExportInfo} from '../declarations/export-info.class';
import {IsId, ValidateObject} from '../../common/decorators';

export class GenerateExportDto extends GenerateBaseDto {
    @ValidateObject(ExportInfo)
    data: ExportInfo;

    @IsId()
    empresaId: number;
}