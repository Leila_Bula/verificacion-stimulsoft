import {IsDefined, IsEnum, IsOptional, IsString} from 'class-validator';
import {ReportFormat} from '../declarations';

export class GenerateBaseDto {
    @IsEnum(ReportFormat)
    @IsDefined()
    type: ReportFormat;

    @IsString()
    @IsOptional()
    docName?: string;
}