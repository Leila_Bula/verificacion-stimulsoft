import {IsId} from '../../common/decorators';
import {IsDefined} from 'class-validator';
import {GenerateBaseDto} from './generate-base.dto';

export class GenerateReportDto extends GenerateBaseDto{
    @IsId()
    @IsDefined()
    templateId: number;

    @IsId()
    @IsDefined()
    empresaId: number;

    @IsId()
    @IsDefined()
    usuarioId: number;
}