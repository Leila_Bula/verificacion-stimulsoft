import {IsArray, IsDefined} from 'class-validator';
import {type DataCampos} from '../declarations';
import {IsId} from '../../common/decorators';

export class GetDataDto {
    @IsArray()
    @IsDefined()
    query: DataCampos[];

    @IsId()
    empresaId: number;

    constructor(query: DataCampos[], empresaId: number) {
        this.query = query;
        this.empresaId = empresaId;
    }
}