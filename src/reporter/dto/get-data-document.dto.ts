import {DocumentTypeEnum} from '../../common/declarations/document-type.enum';
import {IsId} from '../../common/decorators';
import {IsEnum, IsIn, IsOptional, IsString} from 'class-validator';

export class GetDataDocumentDto {
    @IsId()
    empresaId: number;

    @IsOptional()
    @IsEnum(DocumentTypeEnum)
    documento: DocumentTypeEnum;

    @IsString()
    @IsIn(['MAESTROS','DOCUMENTOS'])
    tipoReporte: string;
}