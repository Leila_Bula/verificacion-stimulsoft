import {Module} from '@nestjs/common';
import {ConfigModule} from '@nestjs/config';
import {ReporterController} from './reporter.controller';
import {ReporterService} from './services/reporter.service';
import {StimulsoftService} from './services/stimulsoft.service';

@Module({
    controllers: [ReporterController],
    providers: [ReporterService, StimulsoftService],
    imports: [
        ConfigModule,
    ],
})
export class ReporterModule {
}
