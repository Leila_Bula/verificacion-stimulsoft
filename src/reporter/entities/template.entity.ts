import {Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, RelationId} from 'typeorm';
import {DataCampos} from '../declarations';
import {Empresa} from './empresa.entity';

export interface QueryTemplate {
    query?: DataCampos[];
    filters?: object;
    campos?: string[];
}

@Entity({name: 'template', schema: 'report'})
export class Template {
    @PrimaryGeneratedColumn({type: 'bigint', name: 'id'})
    id: number;

    @Column({type: 'text'})
    nombre: string;

    @Column({type: 'jsonb'})
    data: object;

    @Column({
        type: 'jsonb',
        nullable: false,
        default: () => '\'{}\'::jsonb',
    })
    query: QueryTemplate;

    @Column({
        type: 'timestamp without time zone',
        name: 'created_at',
        default: () => 'now()',
    })
    createdAt: Date;

    @Column({
        type: 'timestamp without time zone',
        name: 'updated_at',
        default: () => 'now()',
    })
    updatedAt: Date;

    @Column({name: 'is_document', type: 'boolean', nullable: false, default: false})
    isDocument: boolean;

    @ManyToOne(() => Empresa)
    @JoinColumn([{name: 'empresa_id', referencedColumnName: 'id'}])
    empresa: Empresa;

    @RelationId((t: Template) => t.empresa)
    empresaId: number;
}
