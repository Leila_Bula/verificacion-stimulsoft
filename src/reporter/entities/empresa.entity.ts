import {Column, Entity, Index, PrimaryGeneratedColumn,} from 'typeorm';

@Index('empresa_pkey', ['id'], {unique: true})
@Index('numero_documento_empresa_uk', ['numeroDocumento'], {unique: true})
@Entity({name: 'empresa', schema: 'public'})
export class Empresa {
    @PrimaryGeneratedColumn({type: 'bigint', name: 'id'})
    id: number;

    @Column({
        type: 'character varying',
        name: 'numero_documento',
        unique: true,
        length: 20,
    })
    numeroDocumento: string;

    @Column({
        type: 'character varying',
        name: 'digito_verificacion',
        nullable: true,
        length: 3,
    })
    digitoVerificacion: string | null;

    @Column({type: 'character varying', name: 'razon_social', length: 70})
    razonSocial: string;

    @Column({
        type: 'character varying',
        name: 'direccion',
        nullable: true,
        length: 100,
    })
    direccion: string | null;

    @Column({type: 'character varying', name: 'telefono', nullable: true, length: 10})
    telefono: string | null;

    @Column({
        type: 'text',
        name: 'img',
        default: () => '/erp/img/empresas-img.014e6e0b.svg',
        select: false
    })
    img: string;

    @Column({type: 'boolean', name: 'agente_retencion_iva', nullable: true})
    agenteRetencionIva: boolean | null;

    @Column({type: 'boolean', name: 'agente_retencion_fuente', nullable: true})
    agenteRetencionFuente: boolean | null;

    @Column({type: 'boolean', name: 'gran_contribuyente', nullable: true})
    granContribuyente: boolean | null;

    @Column({type: 'boolean', name: 'responsable_iva', nullable: true})
    responsableIva: boolean | null;

    @Column({type: 'boolean', name: 'enabled', default: () => 'true'})
    enabled: boolean;

    @Column({type: 'bigint', name: 'retecree_id', default: () => '0'})
    retecreeId: string;

    @Column({type: 'boolean', name: 'autoretenedor_fuente', default: () => 'false'})
    autoretenedorFuente: boolean;

    @Column({type: 'boolean', name: 'regimen_simple_tributacion', default: () => 'false'})
    regimenSimpleTributacion: boolean;

    @Column({
        type: 'timestamp without time zone',
        name: 'created_at',
        default: () => 'now()',
    })
    createdAt: Date;

    @Column({
        type: 'timestamp without time zone',
        name: 'updated_at',
        default: () => 'now()',
    })
    updatedAt: Date;

}