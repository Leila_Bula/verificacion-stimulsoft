import {Injectable} from '@nestjs/common';
import {StimulsoftService} from './stimulsoft.service';
import {type GenerateDocumentDto} from '../dto';
import {type Reporte} from '../declarations';

@Injectable()
export class ReporterService {
    constructor(
        private readonly stimulsoftService: StimulsoftService
    ) {
    }

    /*async generateReport(generateReporteDTO: GenerateReportDto): Promise<Reporte> {
        const {templateId, empresaId, usuarioId} = generateReporteDTO;
        let template: Template;
        template = await this.templateRepository.findOne({where: {id: templateId}});
        if (!template) throw new BadRequestException('This templateId is not valid');
        if (!template.isDocument && template.query.query) {
            const data = await this.getData(
                new GetDataDto(template.query.query, empresaId),
                usuarioId
            );
            return await this.stimulsoftService.generateReportWithData(
                template,
                generateReporteDTO.type,
                data,
                generateReporteDTO.docName || 'reporte.pdf'
            );
        }
        return await this.stimulsoftService.generateReport(
            template,
            generateReporteDTO.type,
            generateReporteDTO.docName || 'reporte.pdf'
        );
    }*/

    async generateDocument(generateDocumentDTO: GenerateDocumentDto): Promise<Reporte> {
        return await this.stimulsoftService.generateReportWithData(
            generateDocumentDTO.template,
            generateDocumentDTO.type,
            generateDocumentDTO.data,
            generateDocumentDTO.docName || 'prueba.pdf'
        );
    }
}