import {Injectable, InternalServerErrorException} from '@nestjs/common';
import {Stimulsoft} from '../stimulsoft';
import {type Template} from '../entities';
import {Reporte} from '../declarations';
import {formatName} from '../../common/tools';

@Injectable()
export class StimulsoftService {

    constructor() {
        Stimulsoft.Base.StiLicense.loadFromFile('license.key');
    }

    async generateReport(
        template: Template,
        format: typeof Stimulsoft.Report.StiExportFormat,
        name?: string,
    ): Promise<Reporte> {
        try {
            const report = this.cargarTemplate(template);
            return await this.generateDoc(report, format, name || 'reporte.pdf');
        } catch (error) {
            this.handleError(error, '1');
        }
    }

    async generateReportWithData(
        template: Template,
        format: typeof Stimulsoft.Report.StiExportFormat,
        data: any,
        name: string,
    ): Promise<Reporte> {
        try {
            const report = this.cargarTemplate(template, formatName(name));
            const dataSet = new Stimulsoft.System.Data.DataSet('default');
            dataSet.readJson(data);
            report.dictionary.databases.clear();
            report.regData('default', 'default', dataSet);
            return await this.generateDoc(report, format, name);
        } catch (error) {
            this.handleError(error, '1');
        }
    }

    private async generateDoc(
        report: typeof Stimulsoft.Report.StiReport,
        format: typeof Stimulsoft.Report.StiExportFormat,
        name: string,
    ): Promise<Reporte> {
        try {
            await report.renderAsync2();
            const result = await report.exportDocumentAsync2(format);
            return new Reporte(Buffer.from(result), name);
        } catch (error) {
            this.handleError(error, '2');
        }
    }

    private cargarTemplate(template: Template, name?: string): typeof Stimulsoft.Report.StiReport {
        const report = new Stimulsoft.Report.StiReport();
        report.load(template.data);
        report.reportName = name ?? template.nombre;
        return report;
    }

    private handleError(err: any, root: string): never {
        console.log(err);
        throw new InternalServerErrorException('stimulsoft_service_' + root);
    }
}