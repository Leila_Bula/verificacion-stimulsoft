// eslint-disable-next-line @typescript-eslint/prefer-nullish-coalescing
import {NestFactory} from '@nestjs/core';
import {Logger, ValidationPipe} from '@nestjs/common';
import {DocumentBuilder, SwaggerModule} from '@nestjs/swagger';

import {AppModule} from './app.module';
import {json} from 'express';


async function bootstrap() {
    const app = await NestFactory.create(AppModule, {cors: true});
    const logger = new Logger('Bootstrap');

    app.setGlobalPrefix('api');

    app.use(json({ limit: '50mb' }));

    app.useGlobalPipes(
        new ValidationPipe({
            whitelist: true,
            forbidNonWhitelisted: true,
        }),
    );


    const config = new DocumentBuilder()
        .setTitle('Reporter Erp ResFull Api')
        .setDescription('End Point')
        .setVersion('1.0')
        .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('api', app, document);


    const portDefoult = process.env.PORT ?? 3000;

    await app.listen(portDefoult);
    logger.log(`App running on port ${process.env.PORT ?? portDefoult}`);
}

void bootstrap();
